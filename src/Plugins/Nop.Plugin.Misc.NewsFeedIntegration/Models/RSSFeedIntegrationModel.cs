﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Misc.RSSFeedIntegration.Models
{
    public record RSSFeedIntegrationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.RssUrl")]
        public string RssUrl { get; set; }
        public bool RssUrl_OverrideForStore { get; set; }
        
        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.Item")]
        public string Item { get; set; }
        public bool Item_OverrideForStore { get; set; }
        
        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.Title")]
        public string Title { get; set; }
        public bool Title_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.ShortDescription")]
        public string ShortDescription { get; set; }
        public bool ShortDescription_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.Description")]
        public string Description { get; set; }
        public bool Description_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Misc.RSSFeedIntegration.Fields.CreatedDate")]
        public string CreatedDate { get; set; }
        public bool CreatedDate_OverrideForStore { get; set; }
    }
}
