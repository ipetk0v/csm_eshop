﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Misc.RSSFeedIntegration.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Misc.RSSFeedIntegration.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class RSSFeedIntegrationController : BasePluginController
    {
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly INotificationService _notificationService;
        private readonly ILocalizationService _localizationService;

        public RSSFeedIntegrationController(
            ISettingService settingService,
            IStoreContext storeContext,
            INotificationService notificationService,
            ILocalizationService localizationService
            )
        {
            _settingService = settingService;
            _storeContext = storeContext;
            _notificationService = notificationService;
            _localizationService = localizationService;
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public async Task<IActionResult> Configure()
        {
            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var rssFeedIntegrationSettings = await _settingService.LoadSettingAsync<RSSFeedIntegrationSettings>(storeScope);
            var storeId = await _storeContext.GetActiveStoreScopeConfigurationAsync();

            var model = new RSSFeedIntegrationModel
            {
                RssUrl = rssFeedIntegrationSettings.RssUrl,
                Item = rssFeedIntegrationSettings.Item,
                Description = rssFeedIntegrationSettings.Description,
                Title = rssFeedIntegrationSettings.Title,
                ShortDescription = rssFeedIntegrationSettings.ShortDescription,
                ActiveStoreScopeConfiguration = storeId,
                CreatedDate = rssFeedIntegrationSettings.CreatedDate
            };

            if (storeScope > 0)
            {
                model.RssUrl_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.RssUrl, storeScope);
                model.Item_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.Item, storeScope);
                model.Description_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.Description, storeScope);
                model.ShortDescription_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.ShortDescription, storeScope);
                model.Title_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.Title, storeScope);
                model.CreatedDate_OverrideForStore = await _settingService.SettingExistsAsync(rssFeedIntegrationSettings, x => x.CreatedDate, storeScope);
            }

            return View("~/Plugins/Misc.RSSFeedIntegration/Views/Configure.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        [HttpPost, ActionName("Configure")]
        [FormValueRequired("save")]
        public async Task<IActionResult> Configure(RSSFeedIntegrationModel model)
        {
            if (!ModelState.IsValid)
                return await Configure();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var storeId = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var rssFeedIntegrationSettings = await _settingService.LoadSettingAsync<RSSFeedIntegrationSettings>(storeId);

            rssFeedIntegrationSettings.RssUrl = model.RssUrl;
            rssFeedIntegrationSettings.Title = model.Title;
            rssFeedIntegrationSettings.Item = model.Item;
            rssFeedIntegrationSettings.Description = model.Description;
            rssFeedIntegrationSettings.ShortDescription = model.ShortDescription;
            rssFeedIntegrationSettings.CreatedDate = model.CreatedDate;

            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.RssUrl, model.RssUrl_OverrideForStore, storeScope, false);
            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.Title, model.Title_OverrideForStore, storeScope, false);
            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.Item, model.Item_OverrideForStore, storeScope, false);
            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.Description, model.Description_OverrideForStore, storeScope, false);
            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.ShortDescription, model.ShortDescription_OverrideForStore, storeScope, false);
            await _settingService
                .SaveSettingOverridablePerStoreAsync(rssFeedIntegrationSettings, x => x.CreatedDate, model.CreatedDate_OverrideForStore, storeScope, false);
            
            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            return await Configure();
        }
    }
}
