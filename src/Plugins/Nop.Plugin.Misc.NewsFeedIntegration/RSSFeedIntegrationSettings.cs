﻿using System;
using Nop.Core.Configuration;

namespace Nop.Plugin.Misc.RSSFeedIntegration
{
    public class RSSFeedIntegrationSettings : ISettings
    {
        public string RssUrl { get; set; }

        public string Item { get; set; }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string Description { get; set; }

        public string CreatedDate { get; set; }
    }
}
