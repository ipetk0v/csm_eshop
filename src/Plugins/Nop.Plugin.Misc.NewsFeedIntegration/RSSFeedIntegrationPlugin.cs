﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Tasks;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Services.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Nop.Plugin.Misc.RSSFeedIntegration
{
    public class RSSFeedIntegrationPlugin : BasePlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly ILocalizationService _localizationService;

        public RSSFeedIntegrationPlugin(
            IWebHelper webHelper,
            IScheduleTaskService scheduleTaskService,
            ILocalizationService localizationService
        )
        {
            _webHelper = webHelper;
            _scheduleTaskService = scheduleTaskService;
            _localizationService = localizationService;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/RSSFeedIntegration/Configure";
        }

        public override async Task InstallAsync()
        {
            //install synchronization task
            if (await _scheduleTaskService.GetTaskByTypeAsync("Nop.Plugin.Misc.RSSFeedIntegration.Services.RSSFeedIntegrationTask") == null)
            {
                await _scheduleTaskService.InsertTaskAsync(new ScheduleTask
                {
                    Enabled = true,
                    //24 hours
                    Seconds = 24 * 60 * 60,
                    Name = nameof(RSSFeedIntegrationPlugin),
                    Type = "Nop.Plugin.Misc.RSSFeedIntegration.Services.RSSFeedIntegrationTask"
                });
            }

            //locales
            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Misc.RSSFeedIntegration.Fields.RssUrl"] = "RSS url",
                ["Plugins.Misc.RSSFeedIntegration.Fields.Item"] = "Item element",
                ["Plugins.Misc.RSSFeedIntegration.Fields.Title"] = "Title element",
                ["Plugins.Misc.RSSFeedIntegration.Fields.Description"] = "Description element",
            });

            await base.InstallAsync();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        public override async Task UninstallAsync()
        {
            //schedule task
            var task = await _scheduleTaskService.GetTaskByTypeAsync("Nop.Plugin.Misc.RSSFeedIntegration.Services.RSSFeedIntegrationTask");
            if (task != null)
                await _scheduleTaskService.DeleteTaskAsync(task);

            //locales
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Misc.RSSFeedIntegration");

            await base.UninstallAsync();
        }
    }
}
