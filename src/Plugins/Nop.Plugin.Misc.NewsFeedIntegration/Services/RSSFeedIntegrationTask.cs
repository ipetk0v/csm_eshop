﻿using Nop.Services.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Nop.Plugin.Misc.RSSFeedIntegration.Services
{
    public class RSSFeedIntegrationTask : IScheduleTask
    {
        private readonly IRSSFeedIntegrationServices _rssFeedIntegrationServices;

        public RSSFeedIntegrationTask(
            IRSSFeedIntegrationServices rssFeedIntegrationServices
            )
        {
            _rssFeedIntegrationServices = rssFeedIntegrationServices;
        }

        public async Task ExecuteAsync()
        {
            await _rssFeedIntegrationServices.Execute();
        }
    }
}
