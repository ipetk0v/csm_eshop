﻿using System.Threading.Tasks;

namespace Nop.Plugin.Misc.RSSFeedIntegration.Services
{
    public interface IRSSFeedIntegrationServices
    {
        public Task Execute();
    }
}
