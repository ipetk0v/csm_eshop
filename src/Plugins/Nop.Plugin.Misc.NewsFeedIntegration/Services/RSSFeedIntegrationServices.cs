﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Nop.Core;
using Nop.Core.Domain.News;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.News;
using Nop.Services.Seo;

namespace Nop.Plugin.Misc.RSSFeedIntegration.Services
{
    public class RSSFeedIntegrationServices : IRSSFeedIntegrationServices
    {
        private readonly RSSFeedIntegrationSettings _rssFeedIntegrationSettings;
        private readonly ILogger _logger;
        private readonly INewsService _newsService;
        private readonly IStoreContext _storeContext;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;

        public RSSFeedIntegrationServices(
            RSSFeedIntegrationSettings rssFeedIntegrationSettings,
            ILogger logger,
            INewsService newsService,
            IStoreContext storeContext,
            IUrlRecordService urlRecordService,
            ILanguageService languageService
            )
        {
            _rssFeedIntegrationSettings = rssFeedIntegrationSettings;
            _logger = logger;
            _newsService = newsService;
            _storeContext = storeContext;
            _urlRecordService = urlRecordService;
            _languageService = languageService;
        }

        public async Task Execute()
        {
            var result = _rssFeedIntegrationSettings.RssUrl;
            var xmlDoc = new XmlDocument(); // Create an XML document object
            var store = await _storeContext.GetCurrentStoreAsync();
            var languageId = (await _storeContext.GetCurrentStoreAsync()).DefaultLanguageId;
            var createdDate = DateTime.Now;

            if (languageId == 0)
                languageId = (await _languageService.GetAllLanguagesAsync(storeId: store.Id)).FirstOrDefault().Id;

            var news = await _newsService.GetAllNewsAsync(languageId, store.Id, showHidden: true);

            using (var xmlReader = XmlReader.Create(result))
            {
                xmlDoc.Load(xmlReader);
                if (xmlDoc.HasChildNodes)
                {
                    XmlNodeList xmlNL = xmlDoc.GetElementsByTagName(_rssFeedIntegrationSettings.Item);
                    foreach (XmlNode xn in xmlNL)
                    {
                        var title = xn[_rssFeedIntegrationSettings.Title]?.InnerText.Trim();
                        var shortDescription = xn[_rssFeedIntegrationSettings.ShortDescription]?.InnerText.Trim();
                        var description = xn[_rssFeedIntegrationSettings.Description]?.InnerText.Trim();
                        var date = xn[_rssFeedIntegrationSettings.CreatedDate]?.InnerText.Trim();

                        if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(description))
                            continue;

                        if(news.FirstOrDefault(n => n.Title.Contains(title)) != null)
                            continue;

                        if (!string.IsNullOrEmpty(date))
                            createdDate = DateTime.Parse(date);

                        var newsItem = new NewsItem
                        {
                            Title = title,
                            Short = shortDescription,
                            Full = description,
                            CreatedOnUtc = createdDate,
                            AllowComments = true,
                            Published = true,
                            LanguageId = languageId
                        };
                        await _newsService.InsertNewsAsync(newsItem);

                        //search engine name
                        var seName = await _urlRecordService.ValidateSeNameAsync(newsItem, string.Empty, newsItem.Title, true);
                        await _urlRecordService.SaveSlugAsync(newsItem, seName, newsItem.LanguageId);
                    }
                }
                else
                {
                    await _logger.ErrorAsync("XmlAutomationImportService => xmlDoc.HasChildNodes not exist child nodes!");
                }
                xmlReader.Dispose();
            }
        }
    }
}
